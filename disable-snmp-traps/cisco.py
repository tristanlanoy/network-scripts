# Disable SNMP traps on Cisco IOS switch

from netmiko import SSHDetect, ConnectHandler
from getpass import getpass

#Port desc, where snmp should be disabled
port_desc = ['Server','Computer','Access']

#It's faster to lookup in a set 
port_desc_set = set(port_desc)

port_no_snmp = []

device = {
    "device_type": "cisco_ios_telnet",
    "host": "localhost",
    "username": "cisco",
    "password": "cisco",
	"port": "5000"
}



def guess_model() :
	""" Autodetect device_type"""
	guesser = SSHDetect(**device)
	guessed_model = guesser.autodetect()
	print ("Detected switch vendor : " + guessed_model)
	return guessed_model


def get_desc_ios() :
	"""Get interfaces and description"""
	command = "show int status"
	return command


command = get_desc_ios()
with ConnectHandler(**device) as net_connect:
	output = net_connect.send_command(command, use_textfsm=True)

print(output)

for port in output :
	print (" \n Nom de port : " + port['port'])
	print ("Description du port : "+port['name'])
	if port['name'] == "Thomas le bg":
		print ("C'est un port à désactiver")
		port_no_snmp.append(port['port'])
	else :
		print ("Ce port est à laisser activé")
print(" \n Liste des ports")
print(*port_no_snmp)
	


